import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService, YesNoResponse, AnswerEnum } from './api.service';
import { map, filter, tap, share } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
    <h1>
      {{ question }}
    </h1>

    <div
      *ngIf="response$ | async as response; else loading;"
    >
      <h2>
        {{ answer$ | async | titlecase }}
      </h2>

      <img
        [src]="response.image"
        [class.loading]="!imgLoaded"
        (load)="loaded()"
      />
    </div>
    <ng-template #loading>
      <h3>
        Hmmm...
      </h3>
    </ng-template>
    <button
      [routerLink]="'/'"
    >
      New question
    </button>
  `
})
export class AnswerComponent implements OnInit, OnDestroy {
  
  public constructor(
    private apiService: ApiService, 
    private route: ActivatedRoute
  ) {}

  subscriptions = [];

  question: string;
  force: string;
  response$: Observable<YesNoResponse>;
  answer$: Observable<string>;

  imgLoaded = false;

  yesAlt = [
    "Obviously yes",
    "Fuck yeah",
    "Oh god yes",
    "Yeah I think so",
    "Of course, yes",
    "Oh absolutely"
  ];

  noAlt = [
    "Fuck no",
    "Hahaha no",
    "I dont think so mate",
    "Are you dumb? Obviously no",
    "Oh honey, no",
    "Oh god no, are you kidding?"
  ];

  ngOnInit() {

    // get url parameter
    const params = this.route.queryParams.pipe(
      filter(params => params.q || (params.q && params.force))
    )
    
    const sub = params.subscribe(params => {
      this.question = params.q;
      this.force = params.force;
    });

    this.subscriptions.push(sub)

    this.response$ = this.apiService.getAnswer(<AnswerEnum> this.force);
    this.answer$ = this.response$.pipe(
      map(response => response.answer),
      map(answer => {
        if (answer == AnswerEnum.YES) {
          const index = Math.round(Math.random() * (this.yesAlt.length - 1));
          return this.yesAlt[index];

        } else if (answer == AnswerEnum.NO) {
          const index = Math.round(Math.random() * (this.noAlt.length - 1));
          return this.noAlt[index];

        } else {
          return answer;
        }
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  loaded() {
    this.imgLoaded = true;
  }
}
