import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'src/environments/environment';
import { map, tap, publishReplay, refCount } from 'rxjs/operators'
import { Observable } from 'rxjs';

export interface YesNoResponse {
  answer: AnswerEnum,
  forced: boolean,
  image: string,
}
export enum AnswerEnum {
  YES = 'yes',
  NO = 'no',
  MAYBE = 'maybe'
}


const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: Http) {}

  public getAnswer(force?: AnswerEnum): Observable<YesNoResponse> {
    const param = force ? `?force=${force}` : '';
    return this.http.get(API_URL + param).pipe(
      map(response => response.json()),
      tap(response => console.log(response)),
      publishReplay(1),
      refCount()
    );
  }
}
