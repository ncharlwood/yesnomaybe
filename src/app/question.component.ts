import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Observable, fromEvent, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { AnswerEnum } from './api.service';

export class QuestionValidators {
  static checkIfQuestion(control: AbstractControl) {
    const regexp = /([^]*)\?$/g; // https://regexr.com/ to test this or check stuff
    const valid = regexp.test(control.value.trim());
    return valid ? null : { invalidQuestion: true} ;
  }
}

@Component({
  selector: 'app-root',
  template: `
    <h1>
      Ask your question:
    </h1>

    <form [formGroup]="form"
      (ngSubmit)="askQuestion()"
    >
      <input 
        type="text"
        formControlName="q"
        id="inputField"
        (input)="resetErrorMessages()"
      />
      <div
        class="errorMsg"
        *ngIf="inputIsEmpty"
      >
        How does one answer a question that doesnt exist?
      </div>
      <div
        class="errorMsg"
        *ngIf="inputNotQuestion"
      >
        Shouldn't a question end with a question mark?
      </div>
      <button
        class="ask"
        type="submit"
      >
        Ask!
      </button>
    </form>
  `,

})
export class QuestionComponent implements OnInit, OnDestroy {
  inputIsEmpty = false;
  inputNotQuestion = false;

  secretKey: AnswerEnum;

  keyEventsSubscription: Subscription;

  form = this.fb.group ({
    q: ['', [Validators.required, QuestionValidators.checkIfQuestion]]
  });

  constructor(
    private router: Router,
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    const keyEvent$ = <Observable<KeyboardEvent>> fromEvent(document, 'keydown');
    this.keyEventsSubscription = keyEvent$.pipe(
      filter(kb => (
          kb.key.toLowerCase() === 'y' || 
          kb.key.toLowerCase() === 'n' ||
          kb.key.toLowerCase() === 'm'
        ) && (
          kb.altKey ||
          kb.ctrlKey
        )
      ),
      map(kb => {
        switch (kb.key.toLowerCase()) {
          case 'y':
            this.secretKey = AnswerEnum.YES;
            break;

          case 'n':
            this.secretKey = AnswerEnum.NO;
            break;

          case 'm':
            this.secretKey = AnswerEnum.MAYBE;
            break;
        }
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.keyEventsSubscription.unsubscribe();
  }

  askQuestion() {
    if (this.form.valid) {
      const params = { ...this.form.value, 'force': this.secretKey };
      this.router.navigate(['/answer'], { queryParams: params });

    } else if (this.empty('q')){
      this.inputIsEmpty = true;

    } else if (this.notQuestion('q')) {
      this.inputNotQuestion = true;

    }
  }

  empty(name: string): boolean {
    return (
      this.form.get(name).hasError('required')
    );
  }

  notQuestion(name: string): boolean {
    return (
      this.form.get(name).hasError('invalidQuestion') && 
      !this.empty('q')
    );
  }

  resetErrorMessages() {
    this.inputIsEmpty = false;
    this.inputNotQuestion = false;
  }
}
